const express = require('express');
const sql = require('./connectToMySQL')

const app = express();

const port = 3000;

const environment = process.env.NODE_ENV;
const apiDescription = process.env.API_DESCRIPTION;

app.get('/', (req, res) => {
    console.log('Solicitud recibida para endpoint GET');
    res.send('Hello World from the API!');
});

app.get('/students', async (req, res) => {
    try {
        const students = await sql.getAllStudents();
        res.send(students);
    } catch (err) {
        console.error(`Error:`, err.message);
    }
});
  
  
app.listen(port, () => {
    console.log(`La aplicacion se esta ejecutando en el ambiente: '${environment}'`);
    console.log(`Descripcion: '${apiDescription}'`)
  
    console.log(`App listening at port: ${port}`);
});