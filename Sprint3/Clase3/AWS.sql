DROP DATABASE IF EXISTS RDS_FirstExample;

CREATE DATABASE RDS_FirstExample;

USE RDS_FirstExample;

CREATE TABLE Students(
    ID INT NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(50) NOT NULL, 
    LastName VARCHAR(50) NOT NULL,
    PhoneNumber INT NOT NULL,
    CONSTRAINT Students_PK PRIMARY KEY (ID)
);

INSERT INTO Students (FirstName, LastName, PhoneNumber)
VALUES ("Alan", "Turing", 123456789),
       ("Bill", "Gates", 85329182),
       ("Ada", "Lovelace", 987654321);