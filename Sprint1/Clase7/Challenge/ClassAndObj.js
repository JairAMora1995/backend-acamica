class Estudiantes{
    constructor (nombre, apellido, Edad, sexo, colorOjos){
        this._nombre = nombre;
        this._apellido = apellido;
        this._Edad = Edad;
        this._sexo = sexo;
        this._colorOjos = colorOjos;
    }

    get nombre(){
        return this._nombre
    }
    set nombre(value){
        this._nombre = value;
    }

    get apellido(){
        return this._apellido
    }
    set apellido(value){
        this._apellido = value;
    }

    get Edad(){
        return this._Edad
    }
    set Edad(value){
        this._Edad = value;
    }

    get sexo(){
        return this._sexo
    }
    set sexo(value){
        this._sexo = value;
    }

    get colorOjos(){
        return this._colorOjos
    }
    set colorOjos(value){
        this._colorOjos = value;
    }

    get TodosLosRasgos(){
        
        return `
        ${this._nombre}
        ${this._apellido}
        ${this._Edad}
        ${this._sexo}
        ${this._colorOjos}
        `;
    }
}

let Estudiante1 = new Estudiantes (`Carmelo`, `Carmelito`, `12`, `Masculino`, `Verdes`);
Estudiante1.TodosLosRasgos 
console.log(Estudiante1);

