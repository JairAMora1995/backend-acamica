//Creacion manual de Objetos.............................
let carro = {
    marca: "BMW", 
    modelo: "x5", 
    anio: 2021,
    color: "Black",
    edad: function () {
        return 2021 - this.anio;
    }
};

carro.tipoCombustible = "Extra";
carro.nombre = function () {
    return `${this.marca}-${this.modelo}`;   
}//Dos maneras de agregar propieda y metedos 

console.log(carro);//Llamado de obj
console.log("Edad: " + carro.edad());//llamado de la fun
console.log("Nombre: " + carro.nombre());


//Creacion de un objeto usando un metodo o "Molde"........
function Usuario(nombreUsuario, nombre, apellido, email, password) {
    this.nombreUsuario = nombreUsuario;
    this.nombre = nombre;
    this.apellido = apellido;
    this.CorreoElectronico= email;
    this.contrasenia = password;
    this.nombreFuncion = function () {
        console.log(`Esto es una funcion creada desde un molde`);
    }
}

let Usuario1 = new Usuario("JeremyRules", "Jere", "Rules", "jeremy@gmail.com", "1234");
let Usuario2 = new Usuario("u1", "Name", "LastName", "emailTuyo", "3214");

console.log(Usuario1);//Imp todo el usuario
console.log(Usuario1.apellido);//Imp solo una propiedad
console.log(Usuario1.nombreFuncion());//Imp La funcion

console.log(Usuario2);

//Creacion de un objeto usando una clase....................
class Moto {
    constructor(marca, modelo, color, anio, cilindraje) {
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.anio = anio;
        this.cilindraje = cilindraje;
    }

    obtenerNombre(){
        return `La moto marca ${this.marca} con cilindraje ${this.cilindraje}`;
    }

    establecerNombre(nombre){
        this.marca = nombre;
    }
}

let honda = new Moto("Honda", "CBR", "negro", 2021, 200);
honda.color = "Rojo";
honda.establecerNombre("Kawa");//Manera de Set!
honda.obtenerNombre(honda.obtenerNombre());//Manera de Get!

console.log(honda); 



