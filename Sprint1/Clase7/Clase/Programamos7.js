class Persona{
    constructor(nombre, apellido, edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    fullName (){
        return `${this.nombre} ${this.apellido}`;
    }

    es_mayor(){
        return this.edad >= 18;
    }
}

let persona1 = new Persona("Carmelo", "Carmelito", 20);

console.log(persona1.fullName());
console.log(persona1.es_mayor());







