const express = require ('express');
const app =  express();
const {obtenerTelefonos, obtenerMitadTelefonos, precioBajoTelefonos, precioAltoTelefonos, agrupacionGamas} = require('./modelos/Telefonos');


app.get('/telefonos', (req, res)=> {
    res.json(obtenerTelefonos());
});

app.get('/mitadtelefonos', (req, res)=>{
    res.json(obtenerMitadTelefonos());
});

app.get('/bajopreciotelefonos', (req, res)=>{
    res.json(precioBajoTelefonos());
});

app.get('/altopreciotelefonos', (req, res)=>{
    res.json(precioAltoTelefonos());
});

app.get('/agrupacionGamas', (req, res) => {
    res.json(agrupacionGamas());
});



app.listen(3000, () =>{
    console.log('Escuchando PORT 3000');
});
