const telefonos = [
    {
        marca: "Samsung",
        gama: "Alta",
        modelo: "S11",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 1200
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiaomi",
        modelo: "Note 10s",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    },
    {
        marca: "Nokia",
        modelo: "Nokia plus 3",
        gama: "Baja",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 100
    },

];

const obtenerTelefonos = () => {
    return telefonos;
}

const obtenerMitadTelefonos = () => {
    const mitad = Math.round(telefonos.length /2);
    const mitadTelefonos = [];
    for (let index = 0; index < mitad; index++) {
        const telefono = telefonos[index];
        mitadTelefonos.push(telefono);
    }
    return mitadTelefonos;
}

const precioBajoTelefonos = () => {
    let PreciosTel = [];
    
    
    for (let index = 0; index < telefonos.length; index++) {
        const telefono = telefonos[index];
        PreciosTel.push(telefono.precio);
        EconomicoTel = Math.min.apply(null, PreciosTel);
        if(EconomicoTel === 100){
            return telefono;
        }
    }

    //return EconomicoTel;   
}

const precioAltoTelefonos = () => {
    let PreciosTelC = [];
    
    for (let index = 0; index < telefonos.length; index++) {
        const telefono = telefonos[index];
        PreciosTelC.push(telefono.precio);
        CostosoTel = Math.max.apply(null, PreciosTelC);
        if(CostosoTel === 1500){
            return telefono;
        }
    }

    //return CostosoTel;
}

const agrupacionGamas = () => {
    let alta = [];
    let media = [];
    let baja = [];
    let todas = [];
    for (let index = 0; index < telefonos.length; index++) {
        const telefono = telefonos[index];
        if(telefono.gama == "Alta"){
            alta.push(telefono);
        }else if (telefono.gama == "Media"){
            media.push(telefono);
        }else if (telefono.gama == "Baja"){
            baja.push(telefono);
        }else{console.log('gama no disponible')}
    }
    todas.push(alta);
    todas.push(media);
    todas.push(baja);
    return todas;
}


module.exports= {obtenerTelefonos, obtenerMitadTelefonos, precioBajoTelefonos, precioAltoTelefonos, agrupacionGamas};