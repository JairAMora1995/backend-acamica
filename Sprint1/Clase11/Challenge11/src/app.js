const { text } = require('express');
const express = require('express')
const app = express()
const fs = require('fs');

let Compas = [];
 
app.get('/ListaCompas', function (req, res) {

    class Estudiantes{
        constructor (nombre, apellido, Edad, sexo, pais){
            this._nombre = nombre;
            this._apellido = apellido;
            this._Edad = Edad;
            this._sexo = sexo;
            this._pais = pais;
        }
    
        get nombre(){
            return this._nombre
        }
        set nombre(value){
            this._nombre = value;
        }
    
        get apellido(){
            return this._apellido
        }
        set apellido(value){
            this._apellido = value;
        }
    
        get Edad(){
            return this._Edad
        }
        set Edad(value){
            this._Edad = value;
        }
    
        get sexo(){
            return this._sexo
        }
        set sexo(value){
            this._sexo = value;
        }
    
        get pais(){
            return this.pais
        }
        set pais(value){
            this._pais = value;
        }
    
        get TodosLosRasgos(){
            
            return `
            ${this._nombre}
            ${this._apellido}
            ${this._Edad}
            ${this._sexo}
            ${this._pais}
            `;
        }
    }
    
    let Estudiante1 = new Estudiantes (`Carmelo`, `Carmelito`, `16`, `Masculino`, `Colombia`);
    Estudiante1.TodosLosRasgos 

    let Estudiante2 = new Estudiantes (`Andres`, `Hurtado`, `26`, `Masculino`, `Alemania`);
    Estudiante1.TodosLosRasgos 

    let Estudiante3 = new Estudiantes (`Andreina`, `Ronate`, `20`, `Femenino`, `Brasil`);
    Estudiante1.TodosLosRasgos 

    Compas.push(Estudiante1, Estudiante2, Estudiante3);

    //Para archivo .txt
    Compas.forEach((Compa,i)=>{
        const text = `Companiero ${i+1}: ${Compa.TodosLosRasgos}\n`;
        console.log(text);
        fs.appendFileSync('ListaCompas.txt',text);
    })

    res.send(Compas)
})


 
app.listen(3000)