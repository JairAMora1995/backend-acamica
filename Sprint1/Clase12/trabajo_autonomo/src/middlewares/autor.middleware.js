const { ObtenerAutores } = require("../models/Autores");

function verifAutores(req, res, next) {
    // let hola = 'Hola desde middlewares';
    // console.log(hola);
    // next();

    const {id}  = req.params;
    const VerifiAutor = ObtenerAutores();
    const filtroM = VerifiAutor.find(u => u.id == id);
    console.log(filtroM.id);//verificacion en consola de numero id

    if (filtroM.id == VerifiAutor.id){
        res.json('El autor no existe en nuestra lista');
    }else {
        next();
    };
};


module.exports = verifAutores;