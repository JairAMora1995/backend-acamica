const express = require('express');
const app = express();
app.use(express.json());

//requerimientos de archivos en folders correspondientes
const autorRoutes = require('./routes/Autor.ruta');

//rutas
app.use('/autores', autorRoutes);



app.listen(3000, ()=> {console.log("Escuchando el puerto 3000")});