const express = require('express');
const router = express.Router();
const {ObtenerAutores, NuevoAutor}= require('../models/Autores');
const autorMiddlew = require('../middlewares/autor.middleware')


const autores = ObtenerAutores();

//Realizacion Get info de Autores
router.get('/', (req,res) =>{
    res.json(ObtenerAutores());
});

router.get('/:id', autorMiddlew, (req, res) =>{
    const {id} = req.params;
    const filtro = autores.find(u => u.id == id)
    res.json(filtro);
});


//Agregar un nuevo autor al Array
router.post('/', (req, res) => {
    const {nombre, apellido, fechaDeNacimiento, libros} = req.body;

    if(nombre && apellido && fechaDeNacimiento && libros){
        const Autor = NuevoAutor(req.body);
        res.json(Autor);
    }else{
        res.json('No se agrego el nuevo autor');
    };
});

//Manera de actualizacion de autores
router.put('/:id', autorMiddlew, (req, res) => {
    const {id} = req.params;
    const ActAutor = req.body;
    autores.splice(id-1, 1, ActAutor);
    res.json(autores);
    console.log('El Autor ha sido actualizado exitosamente');
});

//Manera de eliminar autores
router.delete('/:id', autorMiddlew, (res, req) => {
    const {id} = req.query;
    autores.splice(id, 1);
    res.json('El autor ha sido eliminado');
});

//Generacion y adquisicion informacion relacionada al array de libros 
router.get('/:id/libros/:idLibro', (req,res) =>{
    const {id,idLibro}= req.params;
    const filtro = autores.find(u => u.id == id);
    const filtroLibro = filtro.libros.find( u => u.id == idLibro)
    res.json(filtroLibro); 
});

router.post('/:id/libros/', (req,res)=> {
    const {id} =req.params;
    const filtro = autores.find(u => u.id == id);
    filtro.libros.push(req.body)
    res.json(filtro)
});

router.put('/:id/libros/:idLibro', (req,res) => {
    const {id,idLibro}= req.params;
    const filtro = autores.find(u => u.id = id);
    const ActAutor = req.body
    filtro.libros.splice(idLibro-1,1, ActAutor);
    res.json(filtro);
    console.log('Autor actualizado');
 });

router.delete('/:id/libros/:idLibro', (req,res) =>{
    const {id,idLibro}= req.params;
    const filtro = autores.find(u => u.id == id);
    filtro.libros.splice(idLibro-1,1)
    res.json(filtro); 
});

module.exports = router;