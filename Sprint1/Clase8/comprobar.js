let listaDeUsuarios = []; //Arreglo para los usuarios registrados

document.getElementById("formularioRegistro").addEventListener("submit", (e) => //Se leen los valores ingresados en el formulario de registro
{
    e.preventDefault();
    
    let correoIngresado = document.getElementById("correo").value;                              //Correo ingresado por el usario
    let correoRegistrado = comprobarCorreo(correoIngresado);                                    //Función 'comprobarCorreo' comprueba si el correo existe en el arreglo
    let contrasenaIngresada = document.getElementById("contrasena").value;                      //Contraseña ingresada por el usuario
    let confirmacionContrasena = document.getElementById("confiContrasena").value;              //Contraseña ingresada por el usuario(Repetir contraseña)
    let contrasenaCoincide = contrasenasIguales(contrasenaIngresada, confirmacionContrasena);   //Función 'contrasenaIguales' comprueba que las contraseñas coincidan

    if(!correoRegistrado && contrasenaCoincide) //Se comprueba que el correo no esté registrado y que las contraseñas coincidan
    {
        let nombre = document.getElementById("nombre").value;       //Se leen los valores de los otros campos
        let apellido = document.getElementById("apellido").value;
        let pais = document.getElementById("pais").value;

        let usuario = new Usuarios(nombre, apellido, pais, correoIngresado, contrasenaIngresada); //Se crea un nuevo usuario
        listaDeUsuarios.push(usuario);  //Se agrega el nuevo usuario al arreglo
    }

    console.log(listaDeUsuarios);
});

document.getElementById("formularioIngreso").addEventListener("submit", (e) => //Se leen los valores ingresados en el formulario de ingreso
{
    e.preventDefault();
    
    let correoIngresado = document.getElementById("correoIngreso").value;   //Correo ingresado por el usario
    let correoRegistrado = comprobarCorreo(correoIngresado);                //Se comprueba que el usuario exista (se devuelve true/false y el indice en caso de existir)

    if(correoRegistrado)    //Si el correo está registrado se comprueba que la contraseña sea la que corresponda a dicho correo
    {
        let contrasenaIngresada = document.getElementById("contrasenaIngreso").value;           //Contraseña ingresado por el usario
        let indiceRegistrado = indiceUsuario(correoIngresado);     
        let contrasenaRegistrada = listaDeUsuarios[indiceRegistrado].clave;                        //Contraseña para el correo ingresado
        let contrasenaCoincide = contrasenasIguales(contrasenaIngresada, contrasenaRegistrada); //Se comprueba que la contraseña ingresada coincida con la registrada
             
        if(contrasenaCoincide)  //Si la contraseña coincide retorna el indice del usuario en el arreglo
        {
            window.alert(indiceRegistrado);
        }
        else                    //Si la contraseña no coincide retorna false
        {
            window.alert(false);
        }
    }
    else                    //Si el correo no está registrado retorna false
    {
        window.alert(false);
    }

    console.log(listaDeUsuarios);
});

class Usuarios  //Clase usuario con valores de registro
{
    constructor(nombre, apellido, pais, correo, clave)
    {
        this.nombre = nombre;
        this.apellido = apellido;
        this.pais = pais;
        this.correo = correo;
        this.clave = clave;
    }
}

function comprobarCorreo(direccion) //Función que comprueba que el correo ingresado esté en el arreglo de usuarios registrados
{
    let encontrado = true;
    let indice = listaDeUsuarios.findIndex(usuario => usuario.correo === direccion);

    if(-1 !== indice)
    {
        return encontrado;
    }
    else
    {
        return !encontrado;
    }
}

function indiceUsuario(direccion) //Función que comprueba que el correo ingresado esté en el arreglo de usuarios registrados
{
    let indice = listaDeUsuarios.findIndex(usuario => usuario.correo === direccion);

    return indice;
}

function contrasenasIguales(contrasena1, contrasena2) //Función que comprueba que las contraseñas coincidan
{
    if(contrasena1 === contrasena2)
    {
        return true;
    }
    else
    {
        return false;
    }
}
