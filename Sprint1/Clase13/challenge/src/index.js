const express = require('express');     
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const app = express();


const swaggerOptions = {
    swaggerDefinition: {
        info: {
        title: 'Challenge Clase3',
        version: '1.0.0'
            }
                    },
        apis: ['../src/index.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs',
swaggerUI.serve,
swaggerUI.setup(swaggerDocs));

/** 
* @swagger
 * /: 
* get: 
* description: ¡Bienvenido a swagger-jsdoc! 
* respuestas: 
* 200: 
* descripción: Devuelve una cadena misteriosa. 
*/ 
app.get('/estudiantes', (req, res) => {
    res.send([
        { id: 1, nombre: "Menez", edad: 20 },
        {id: 1, nombre: "Paulo", edad: 35}
            ])
}); 


/**
* @swagger
* /estudiantes:
*  post:
*
*/
app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});

/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
  *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */
 app.post('/estudiantes', (req, res) => {
    res.status(201).send();
  });



app.listen(5000, () => console.log("listening on 5000"));

