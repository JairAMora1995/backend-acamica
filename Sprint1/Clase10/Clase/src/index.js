require('dotenv').config();
const env = require('./appsettings.json');
const chalk = require('chalk');

const node_env = process.env.NODE_ENV || 'Development';
const variables = env[node_env];

console.log(chalk.red(node_env));
console.log(JSON.stringify(variables));