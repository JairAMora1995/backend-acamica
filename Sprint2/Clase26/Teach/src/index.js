const express = require('express');
// const { values } = require('sequelize');
// const { Televisores } = require('./models');
const app = express();
const db = require('./models');
const { Op } = require("sequelize");


app.use(express.json());

app.get('/marcas', async (req, res) => {
    const TodasMarcas = await db.Marcas.findAll({
        include: ["televisores"]
    });
    res.json(TodasMarcas);
});

app.get('/modelos', async (req, res) => {
    const TodosModelos = await db.Modelos.findAll();
    res.json(TodosModelos);
});

app.get('/televisores', async (req, res) => {
    const TodosTVs = await db.Televisores.findAll();
    res.json(TodosTVs);
});

app.post('/marca', async (req, res) => {
    const { nombre } = req.body;
    const NuevaMarca = await db.Marcas.create({ nombre });
    res.json(NuevaMarca);
});

app.post('/modelo', async (req, res) => {
    const { referencia, descripcion, marcaId } = req.body;
    const NuevoModelo = await db.Modelos.create({ referencia, descripcion, marcaId });
    res.json(NuevoModelo);
});

app.post('/televisor', async (req, res) => {
    const { precio, pantalla, smart, cantidad, marcaId, modeloId } = req.body;
    const NuevoTv = await db.Televisores.create({ precio, pantalla, smart, cantidad, marcaId, modeloId });
    res.json(NuevoTv);
});

app.get('/marca_modelos/', async (req, res) => {
    const { nombre } = req.body;
    const modeloDeMarca = await db.Marcas.findAll({
        include: ["modelos"],
        where :{
            nombre: nombre
        },
    });
    res.json(modeloDeMarca);
});

app.get('/precioTv_menorQue', async (req, res) =>{
    const { precio } = req.body;
    const PrecioTV = await db.Televisores.findAll({
        where: {
            precio: {
                [Op.lt]: precio
            }
        }
    });
    res.json(PrecioTV);
});

app.get('/precioTv_mayorQue', async (req, res) =>{
    const { precio } = req.body;
    const PrecioTV = await db.Televisores.findAll({
        where: {
            precio: {
                [Op.gt]: precio
            }
        }
    });
    res.json(PrecioTV);
});

app.get('/preciosTv_Ascendente', async (req, res) =>{
    const OrdenarTvs = await db.Televisores.findAll({
        order: [["precio", "ASC"]]
    });
    res.json(OrdenarTvs);
});




db.sequelize.sync({ force: false })
    .then(()=> {
        console.log('This Project connect to DB');
        app.listen(3000);
        console.log('Listen Port 3000');
    })
    .catch(err=> {
        console.log('Erro concect to DB:' +err);
    });

    