const { Sequelize, DataTypes } = require('sequelize');


const sequelize = new Sequelize('Clase26', 'root', 'del0al10',{
    host: 'localhost',
    dialect: 'mysql'
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Televisores = require('./televisor.model')(sequelize, DataTypes);
db.Marcas = require('./marca.model')(sequelize, DataTypes);
db.Modelos = require('./modelo.model')(sequelize, DataTypes);

db.Marcas.hasMany(db.Televisores);
db.Televisores.belongsTo(db.Marcas);

db.Modelos.hasMany(db.Televisores);
db.Televisores.belongsTo(db.Modelos);

db.Marcas.hasMany(db.Modelos);
db.Modelos.belongsTo(db.Marcas);


module.exports = db;