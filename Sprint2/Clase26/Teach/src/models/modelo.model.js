/**
 * Create a new model using sequelize and DataTypes
 * @param {import('sequelize').Sequelize} sequelize 
 * @param {import('sequelize').DataTypes} DataTypes 
 * @returns New model Modelo 
 */
 module.exports = (sequelize, DataTypes) => {
    const Modelo = sequelize.define("modelos", {
        referencia: {
            type: DataTypes.STRING
        },
        descripcion: {
            type: DataTypes.TEXT
        }
    });

    return Modelo;
}
