const mongoose = require('mongoose');
const express = require('express');
const app = express();

app.use(express.json());

(async () => {
    const plato = await require('../models/schema')

    const mongo = await mongoose.connect('mongodb://localhost:27017/Menu',
        {useNewUrlParser: true, useUnifiedTopology: true});
    
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log(`I'm connect with mongodb!`)
    });

    // creacion del plato
    const platoNuevo1 = new plato({nombre:"salchipapa", precio: 9000,
     categoria: "comida rapida"});

    await platoNuevo1.save();
    //console.log(platoNuevo1);

    const platoNuevo2 = new plato({nombre:"platano con queso", precio: 3500,
     categoria: "comida rapida"});

    await platoNuevo2.save();
    // console.log(platoNuevo2);

    app.get('/obtenerPlato', async(req,res) => {
        const platos = await plato.find();
        res.json(platos);
    });

    app.put('/actualizarPlato/:id', async(req,res) =>{
        const platillo = await plato.findById({_id:req.params.id});
        platillo.nombre = req.body.nombre;
        platillo.precio = req.body.precio;
        platillo.save();
        res.json(platillo);
    });

    app.delete('/borrarPlato/:id', async(req, res)=> {
        const platillo = await plato.deleteOne({_id:req.params.id});
        const platos = await plato.find();
        res.json(platos);
    });

    app.post('/agregarPlato', async(req, res) => {
        const {nombre, precio, categoria} = req.body;
        const nuevoPlato = new plato({nombre:nombre, precio:precio, categoria:categoria});
        nuevoPlato.save();
        res.json(await plato.find());
    });

    app.listen(3000, () => {console.log('escuchando en el puerto ' + 3000)})
    
})();