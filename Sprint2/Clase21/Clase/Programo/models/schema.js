const mongoose = require('mongoose');

    const PlatoSchema = new mongoose.Schema({
        nombre:{
            required:true,
            type:String
        },
        precio:{
            required:true,
            type:Number
        },
        categoria:{
            required:true,
            type:String
        }
    });

    const plato = mongoose.model('Plato', PlatoSchema);

module.exports = plato
