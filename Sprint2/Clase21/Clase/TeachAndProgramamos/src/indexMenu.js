const mongoose = require('mongoose');

(async () => {
    const db = await mongoose.connect('mongodb://localhost:27017/acamica1', { useNewUrlParser: true, useUnifiedTopology: true });

    console.log(`I'm connect to BD`);

    const menuSchema = new mongoose.Schema({
        plato: {
            required: true,
            type: String
        },
        precio: {
            required: true,
            type: Number
        },
        categoria: {
            required: true,
            type: String
        }
    });
    const Menu = mongoose.model('Menu', menuSchema);

    // Primer plato
    const primerPlato = new Menu({
        plato: "Hamburguesa",
        precio: 12000,
        categoria: "Comida rapida"
    });
    await primerPlato.save();

    // Segundo plato
    const segundoPlato = new Menu({
        plato: "Chuzo desgranado",
        precio: 12500,
        categoria: "Comida rapida"
    });
    await segundoPlato.save();

    //Retorna todos los platos del restaurante
    const platos = await Menu.find();
    console.log(platos);

})();
