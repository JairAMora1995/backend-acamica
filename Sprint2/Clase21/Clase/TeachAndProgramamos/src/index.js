const mongoose = require('mongoose');

(async () => {
    const db = await mongoose.connect('mongodb://localhost:27017/acamica1', { useNewUrlParser: true, useUnifiedTopology: true });

    console.log(`I'm conect to BD`);

    const usuarioSchema = new mongoose.Schema({
        nombre: {
            type: String,
            required: true
        },
        apellido: {
            type: String,
            required: true
        },
        direccion: String,
        edad: Number,
        esAdministrador: {
            type: Boolean,
            default: false
        }
    });

    const Usuario = mongoose.model('Usuarios', usuarioSchema);

    // Creacion usuario
    // const usuarioNuevo = new Usuario({ nombre: "Jeremy", apellido: "Spoken", direccion: "Direccion prueba", edad: 25, esAdministrador: true });
    // await usuarioNuevo.save();
    //console.log(usuarioNuevo);

    // Buscar todos los usuarios del sistema
    // const usuarios = await Usuario.find(); //Trae todos los usuarios
    // const usuarios = await Usuario.find({ esAdministrador: true }); // Trae usuarios con la propiedad esAdministrador en true
    // const usuarios = await Usuario.findById("60fa0a3361513e2a10e1b414");
    // const usuarios = await Usuario.findOne({ esAdministrador: true });
    // console.log(usuarios);

    // Actualizar usuario
    // const usuario = await Usuario.findById("60fa0a3361513e2a10e1b414");
    // usuario.nombre = "Nombre prueba";
    // usuario.edad = 5;
    // usuario.save();
    // console.log(usuario);

    // Eliminar usuario
    // const result = await Usuario.findByIdAndDelete("60fa0a3361513e2a10e1b414");
    // const result = await Usuario.findOneAndDelete({ esAdministrador: false });
    // console.log(result);
})();
