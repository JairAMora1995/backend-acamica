const sequelize = require('./db');
const { QueryTypes } = require('sequelize');
const express = require('express');
const app = express();
app.use(express.json());


app.get('/bandas', async (req, res) => {
    const Bandas = await sequelize.query( "SELECT * FROM Bandas",
        {
            type: QueryTypes.SELECT
        }
    );
        //console.log(Bandas);
        res.json(Bandas);

});

app.put('/actbandas/:id', async (req, res) => {
    const { Nombre } = req.body;
    const { id } = req.params;
    await sequelize.query( "UPDATE Bandas SET Nombre = ? WHERE id = ?",
        {
            replacements: [Nombre, id],
            type: QueryTypes.UPDATE
        }
    );
        res.status(200).json('Banda exitosamente Actualizada');
});

app.get('/canciones', async (req, res) => {
    const Canciones = await sequelize.query( "SELECT * FROM Canciones",
        {
            type: QueryTypes.SELECT
        }
    );
        //console.log(Bandas);
        res.json(Canciones);
});

app.get('/albumes', async (req, res) => {
    const Albumes = await sequelize.query( "SELECT * FROM Albumes",
        {
            type: QueryTypes.SELECT
        }
    );
        //console.log(Bandas);
        res.json(Albumes);
});

app.delete('/delete_albumes/:id', async (req, res) => {
    try {
        const { id } = req.params;
        await sequelize.query( "DELETE FROM Albumes WHERE id = ?",
            {
                replacements: [id],
                type: QueryTypes.DELETE
            }
        );
        res.status(200).json('Album eliminado')
    } catch (error) {
        res.status(500).json(error);
    }
});

app.post('/post_albumes', async (req, res) => {
    const { Nombre_album, Banda, Fecha_de_publicacion } = req.body;
    await sequelize.query("INSERT INTO Albumes (Nombre_album, Banda, Fecha_de_publicacion) VALUES (?, ?, ?)",
        {
            replacements: [Nombre_album, Banda, Fecha_de_publicacion],
            type: QueryTypes.INSERT
        }
    );
    res.status(201).send();
});

app.put('/act_albumes/:id', async (req, res) => {
    const { Nombre_album, Banda, Fecha_de_publicacion } = req.body;
    const { id } = req.params;
    await sequelize.query("UPDATE Albumes SET Nombre_album = ?, Banda = ?, Fecha_de_publicacion = ? WHERE id = ?",
    {
        replacements: [Nombre_album, Banda, Fecha_de_publicacion, id],
        type: QueryTypes.UPDATE
    }
    )
    res.status(200).json('Album actualizado');
});


app.listen(3000)
console.log("Escuchando el puerto 3000");