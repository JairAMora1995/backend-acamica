-- mi primer manejo MySQL
-- USE programamos;

-- CREATE TABLE Bandas (
--     `id` int NOT NULL AUTO_INCREMENT,
--     `Nombre` varchar(100) NOT NULL,
--     `Integrantes` int NOT NULL, 
--     `Fecha_de_inicio` date NOT NULL,
--     `Fecha_de_separacion` date NULL,
--     `Pais` varchar(100) NOT NULL,
--     PRIMARY KEY (id)
-- );

-- INSERT INTO Bandas (Nombre, Integrantes, Fecha_de_inicio, Fecha_de_separacion, Pais)
-- VALUES ('Orquesta la 33', 14, '2001-07-28', NULL ,'Colombia')

UPDATE marcas SET Nombre = 'Samsung'
WHERE id = 2;

-- CREATE TABLE Canciones (
--     `id` int NOT NULL AUTO_INCREMENT,
--     `Nombre` varchar(100) NOT NULL,
--     `Duracion` int NOT NULL, 
--     `Album` int NOT NULL,
--     `Banda` int NOT NULL,
--     `Fecha_de_publicacion` date NOT NULL,
--     PRIMARY KEY (id)
-- );

-- INSERT INTO Canciones (Nombre, Duracion, Album, Banda, Fecha_de_Publicacion)
-- VALUES ('Dime porque sera', 126, 6, 8,'2019-05-01')

-- CREATE TABLE Albumes (
--     `id` int NOT NULL AUTO_INCREMENT,
--     `Nombre_album` varchar(100) NOT NULL,
--     `Banda` int NOT NULL,
--     `Fecha_de_publicacion` date NOT NULL,
--     PRIMARY KEY (id)
-- );

-- INSERT INTO Albumes (Nombre_album, Banda, Fecha_de_Publicacion)
-- VALUES ('Use Your Illusion II', 1,'1991-09-17')

-- UPDATE Albumes SET Banda = 6
-- WHERE id = 3;

-- obtener todas las bandas
-- SELECT * FROM Bandas;

-- obtener todas las bandas de tu pais
-- SELECT * FROM Bandas WHERE Pais = 'Colombia';

-- Obtener una banda solista
-- SELECT * FROM Bandas WHERE Integrantes = 1

-- Todas las canciones despues del 2015
-- SELECT * FROM Canciones WHERE Fecha_de_publicacion > '2015-12-31';

-- Todas las canciones que duren mas de 3 mins
-- SELECT * FROM Canciones WHERE Duracion > 180;

-- Obtener todos los Albumes
-- SELECT * FROM Albumes;


