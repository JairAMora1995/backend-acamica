const sequelize = require('./db');
const { QueryTypes } = require('sequelize');
const express = require('express');
const app = express();
app.use(express.json());

app.get('/bandas', async (req, res) => {
    const GetBands = await sequelize.query(`SELECT * FROM Bandas`, 
        {
            type: QueryTypes.SELECT
        }
    );
    res.status(201).json(GetBands);
});

app.get('/banda/:id', async (req, res)=> {
    const { id } = req.params;
    const GetBand_Album = await sequelize.query(`SELECT Bandas.Nombre AS 'Nombre_Banda', Albumes.Nombre_album AS 'Nombre_Del_Album', Albumes.id AS 'Album_id', Albumes.Fecha_de_publicacion AS 'Fecha_Lanza_Album' FROM Albumes
    INNER JOIN Bandas
    ON Albumes.Banda = Bandas.id WHERE Bandas.id = ?`,
        {
            replacements: [id],
            type: QueryTypes.SELECT
        }
    );
    res.status(200).json(GetBand_Album)
});

app.get('/album/:id', async (req, res)=> {
    const { id } = req.params;
    const GetAlbum_Songs = await sequelize.query(`SELECT Albumes.id AS 'Album_id', Albumes.Nombre_album AS 'Nombre_Del_Album', Albumes.Fecha_de_publicacion AS 'Lanzamiento_Album',  Canciones.Album AS 'No_Album', Canciones.Nombre AS 'Nombre_Cancion' FROM Albumes
    INNER JOIN Canciones
    ON Albumes.Banda = Canciones.Banda WHERE Albumes.id = ?`,
        {
            replacements: [id],
            type: QueryTypes.SELECT
        }
    );
    res.status(200).json(GetAlbum_Songs)
});

app.get('/cancion/:id', async (req, res)=> {
    const { id } = req.params;
    const GetSong = await sequelize.query(`SELECT * FROM Canciones WHERE id = ?`,
        {
            replacements: [id],
            type: QueryTypes.SELECT
        }
    );
    res.status(200).json(GetSong)
});


app.listen(3000)
console.log('Listening the 3000 port');