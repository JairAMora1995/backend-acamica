-- mi primer manejo MySQL
-- CREATE DATABASE ChallengeBands;

-- USE programamos;

-- CREATE TABLE Bandas (
--     `id` int NOT NULL AUTO_INCREMENT,
--     `Nombre` varchar(100) NOT NULL,
--     `Integrantes` int NOT NULL, 
--     `Fecha_de_inicio` date NOT NULL,
--     `Fecha_de_separacion` date NULL,
--     `Pais` varchar(100) NOT NULL,
--     PRIMARY KEY (id) 
-- );

-- INSERT INTO Bandas (Nombre, Integrantes, Fecha_de_inicio, Fecha_de_separacion, Pais)
-- VALUES ('Maroon 5', 6, '1994-06-8', NULL ,'USA')

-- UPDATE Bandas SET Nombre = 'Joan Sebastian'
-- WHERE id = 7;

-- CREATE TABLE Canciones (
--     `id` int NOT NULL AUTO_INCREMENT,
--     `Nombre` varchar(100) NOT NULL,
--     `Duracion` int NOT NULL, 
--     `Album` int NOT NULL,
--     `Banda` int NOT NULL,
--     `Fecha_de_publicacion` date NOT NULL,
--     PRIMARY KEY (id),
--     FOREIGN KEY (Banda) REFERENCES Bandas(id)
-- );

-- INSERT INTO Canciones (Nombre, Duracion, Album, Banda, Fecha_de_Publicacion)
-- VALUES ('Soledad', 286, 1, 1,'2004-06-22')

-- CREATE TABLE Albumes (
--     `id` int NOT NULL AUTO_INCREMENT,
--     `Nombre_album` varchar(100) NOT NULL,
--     `Banda` int NOT NULL,
--     `Fecha_de_publicacion` date NOT NULL,
--     PRIMARY KEY (id)
--     FOREIGN KEY (id) REFERENCES Persons(PersonID)
-- );

-- INSERT INTO Albumes (Nombre_album, Banda, Fecha_de_Publicacion)
-- VALUES ('Use Your Illusion II', 1,'1991-09-17')

-- UPDATE Canciones SET Nombre = "She will be loved"
-- WHERE id = 2;

-- obtener todas las bandas
-- SELECT * FROM Bandas;

-- obtener todas las bandas de tu pais
-- SELECT * FROM Bandas WHERE Pais = 'Colombia';

-- Obtener una banda solista
-- SELECT * FROM Bandas WHERE Integrantes = 1

-- Todas las canciones despues del 2015
-- SELECT * FROM Canciones WHERE Fecha_de_publicacion > '2015-12-31';

-- Todas las canciones que duren mas de 3 mins
-- SELECT * FROM Canciones WHERE Duracion > 180;

-- Obtener todos los Albumes
-- SELECT * FROM Albumes;


--  Aniadir realciones 

-- ALTER TABLE Albumes 
-- ADD FOREIGN KEY (Banda) REFERENCES Bandas(id);  

-- ALTER TABLE Canciones 
-- ADD FOREIGN KEY (Banda) REFERENCES Bandas(id);  

-- SELECT Bandas.Nombre AS 'Nombre_Banda', Albumes.Nombre_album AS 'Nombre_Del_Album', Albumes.id AS 'Album_id', Albumes.Fecha_de_publicacion AS 'Fecha_Lanza_Album' FROM Albumes 
-- INNER JOIN Bandas
-- ON Albumes.Banda = Bandas.id WHERE Bandas.id = 3

-- SELECT Albumes.id AS 'Album_id', Albumes.Nombre_album AS 'Nombre_Del_Album', Albumes.Fecha_de_publicacion AS 'Lanzamiento_Album',  Canciones.Album AS 'No_Album', Canciones.Nombre AS 'Nombre_Cancion' FROM Albumes
-- INNER JOIN Canciones
-- ON Albumes.Banda = Canciones.Banda WHERE Albumes.id = 3