const express = require('express');
require('./db');
const Orden = require('../models/orden');
const mongoose = require('mongoose');

const app = express();

app.use(express.json());

app.get('/orden', async (req, res) => {
    const ordenes = await Orden.find();
    res.json(ordenes);
});

app.post('/orden', async (req, res) => {
    const { emailUsuario, direccion } = req.body;
    const nuevaOrden = new Orden({ emailUsuario, direccion });
    nuevaOrden.save();
    res.json(nuevaOrden);
});

app.post('/agregarProducto/:idOrden', async (req, res) => {
    const { idOrden } = req.params;
    const { nombre, cantidad, precio } = req.body;

    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const ordenActualizar = await Orden.findById(idOrden);
        ordenActualizar.productos.push({ nombre, cantidad, precio });
        ordenActualizar.save();
        await session.commitTransaction();
        session.endSession();
        res.json(ordenActualizar);
    }
    catch (err) {
        await session.abortTransaction();
        session.endSession();
        res.status(500).json('Internal server error');
    }
});

app.listen(3000);
console.log('Escuchando en el puerto 3000');
