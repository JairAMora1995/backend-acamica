const mongoose = require('mongoose');

    const userBankAccount_Schema = new mongoose.Schema({
        Name: {
            type: String, 
            require: true
        },
        LastName: {
            type: String,
            require: true
        }, 
        Email: {
            type: String,
            require: true
        }, 
        Balance: {
            type: Number,
            default: 0
        }
    });

module.exports = mongoose.model('HomeBanking', userBankAccount_Schema);