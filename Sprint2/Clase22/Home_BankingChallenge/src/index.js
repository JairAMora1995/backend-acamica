const express = require('express');
const app = express();
const mongoose = require('mongoose');
const db = require('./db')
const BankAccount = require('../models/Model_BankAcount');

app.use(express.json());

app.post('/CreateAccount', async (req, res) => {
    const { Name, LastName, Email } = req.body;
    const ExistEmail = await BankAccount.findOne({ Email })

    if( ExistEmail ){
        res.status(400).json(`El Email ingresado ya existe, porfavor ingrese otro`)
    } else{
        const newBankAccount = new BankAccount({ Name, LastName, Email })
        newBankAccount.save();
        res.status(200).json(newBankAccount)
    }
});

app.get('/BankUsers', async (req, res) => {
    res.json(await BankAccount.find());
});

// app.delete('/DeleteUser/:id', async (req, res) =>{
//     const id = await BankAccount.deleteOne({ _id:req.params.id });
//     const usersBank = await BankAccount.find();
//     res.json(usersBank);
// });

app.post('/userPostMoney/:Email', async (req, res) =>{
    const { Email } = req.params;
    const { VirtualMoney } = req.body;

    const ExistEmail = await BankAccount.findOne({ Email })
    if(ExistEmail){
        const session = await mongoose.startSession();
        session.startTransaction();
        try {
            ExistEmail.Balance += parseInt( VirtualMoney );
            ExistEmail.save();
            await session.commitTransaction();
            session.endSession();
            res.json(ExistEmail);
        } catch (error) {
            await session.abortTransaction();
            session.endSession();
            res.status(500).json(`Internal server error`);
        }
    }else{
        res.status(400).json(`El correo no existe`);
    }
});

app.post('/sendMoney/:Email1/to/:Email2', async( req, res) => {
    const  Email1  = req.params.Email1;
    const Email2 = req.params.Email2;
    const { VirtualMoney } = req.body;
    
    const ExistEmail1 = await BankAccount.findOne({ Email:Email1 });
    const ExistEmail2 = await BankAccount.findOne({ Email:Email2 });
    if (ExistEmail1 && ExistEmail2){
        if( ExistEmail1.Balance >= parseInt(VirtualMoney) ){
            const session = await mongoose.startSession();
            session.startTransaction();
            try {
                ExistEmail1.Balance -= parseInt(VirtualMoney);
                ExistEmail2.Balance += parseInt(VirtualMoney);
                ExistEmail1.save();
                ExistEmail2.save();
                await session.commitTransaction();
                session.endSession();
                res.status(200).json(`${ExistEmail1.Name} ${ExistEmail1.LastName} ha enviado un monto de ${VirtualMoney} a ${ExistEmail2.Name} ${ExistEmail2.LastName}`);
            } catch (error) {
                await session.abortTransaction();
                session.endSession();
                res.status(500).json(`Internal server error`);
            }
        }else res.status(400).json(`El monto a transferir supera el saldo de la cuenta del usuario ${ExistEmail1.Name} ${ExistEmail1.LastName}`) 

    }else{ res.status(400).json('El correo del remitente o del destinatario no existe') }

});




app.listen(3000);
console.log('Escuchando el puerto 3000');