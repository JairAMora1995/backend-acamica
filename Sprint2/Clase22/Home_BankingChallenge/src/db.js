const mongoose = require('mongoose');

(async ()=> {
    const db = await mongoose.connect('mongodb://localhost:27017/bankAccount', 
     {useNewUrlParser: true, useUnifiedTopology: true });
     console.log(`This project connect to DB`);
})();