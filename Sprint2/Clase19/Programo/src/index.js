const fetch = require( 'node-fetch' );

//ejemplos Promesas
let promesa1 = new Promise((resolve, reject)=> {
    setTimeout(() => { 
        resolve("Resuleto en 5s") 
    }, 5000);

    setTimeout(() => { 
        reject("fallo en 3s") 
    }, 3000);
});



promesa1
    .then((mensaje) => { 
        console.log(mensaje);
     })
    .catch((mensaje)=> { 
        console.error(mensaje);
     });



let promesa2 = new Promise((resolve, reject) => {
    setTimeout(() => { 
        resolve("Resuleto en 1s desde promesa2") 
    }, 1000);  
});

promesa2 
    .then((msg) =>{
        console.log(msg);
    })

fetch('https://dog.ceo/api/breeds/image/random')
    .then((res)=> {
        res.json()
            .then((jsonResponse) => {
                console.log(jsonResponse);
            })
            .catch(()=> {
                console.log('error');
            });
    })
    .catch(err => {console.log(err);});