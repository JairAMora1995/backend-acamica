const fetch = require('node-fetch');

const cities = ['Bucaramanga', 'Paris', 'Miami', 'Cartagena', 'Cali', 'Quito', 'London', 'Lima', 'Pasto', 'Tunja' ];

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
 const q = 3;
 
for (var i = 0; i < q; i++) {
    const aleatorio = getRandomInt(0,cities.length) ; 
    const city = cities[aleatorio];
    fetch('http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&APPID=7097c8a0c102263a552e997850a5081c')
    .then((res) =>{
        return res.json()
     })
     .then((json) =>{
         const main = json.main;
         console.log(`${city} con temperatura ${parseInt(main.temp)} grados celsius.`);
     })
     .catch(err => {console.log(err)})
}
