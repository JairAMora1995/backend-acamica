//codigo de clase async y await

const fetch = require('node-fetch');

const url = `http://api.openweathermap.org/data/2.5/weather?q=Bogota&units=metric&APPID=7097c8a0c102263a552e997850a5081c`;

// function climaCiudad(nombre) {
//     fetch(url)
//         .then(respuesta => respuesta.json())
//         .then(resultado => console.log(resultado))
//         .cathc(err => console.error(err));
// }

async function climaCiudad() {
    try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        return resultado;
        //console.log(resultado);    
    } catch (error) {
        console.error(error);
    }
    
}

(async function() {
    const clima = await climaCiudad();
    console.log(clima);
})();