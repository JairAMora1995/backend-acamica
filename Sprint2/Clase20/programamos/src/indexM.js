const fetch = require('node-fetch');
const express = require('express');
const app = express();

app.use(express.json());

const apiKey = '82164e92';
const url = `http://www.omdbapi.com/?apikey=${apiKey}&`;


app.get('/peliculas', async (req, res) => {
    const { nombrePelicula } = req.query;
    const respuesta = await fetch(url+`t=${nombrePelicula}`);
    const pelicula = await respuesta.json();
    res.json({ pelicula
        // titulo: pelicula.Title,
        // imagen: pelicula.Poster,
        // descripcion: pelicula.Plot
    });
});


//intento de generar un for tal que cumpla el async
app.post('/printPeliculas', async (req, res) => {

    const PromesaPeliculas = async (nombrePelicula) => {
        const  ListadoPeliculas  = [];
        const respuesta = await fetch(url+`t=${nombrePelicula}`);
        const pelicula  = await respuesta.json();
        ListadoPeliculas.push({ 
            Titulo: pelicula.Title,
            PosterPelicula: pelicula.Poster,
            Descripcion: pelicula.Plot
        });   
        return ListadoPeliculas ;
    }

        const { peliculas } = req.body; 
        const promesas = peliculas.map(pelicula => PromesaPeliculas(pelicula));
        const ListadoPeliculas = await Promise.all(promesas)
        //console.log(promesas);
        res.json({ ListadoPeliculas });    
   
});


app.listen(3000);
console.log('Escuchando el puerto 3000');